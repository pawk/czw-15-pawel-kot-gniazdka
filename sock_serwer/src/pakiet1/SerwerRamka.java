package pakiet1;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;

public class SerwerRamka extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JPanel contentPane;
	static PanelObraz panel = new PanelObraz();
	static Toolkit toolkit = Toolkit.getDefaultToolkit();
	static Clipboard schowek = toolkit.getSystemClipboard();
	private static JTextArea textArea;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SerwerRamka frame = new SerwerRamka();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		try {
			ServerSocket sSock = new ServerSocket(6666);
			Socket sock = sSock.accept();
			DataInputStream in = new DataInputStream(sock.getInputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(in));
			int liczba = in.readInt();

			if(liczba == 0)
			{
				String tekst;
				DataInputStream in1 = new DataInputStream(sock.getInputStream());
				BufferedReader is1 = new BufferedReader(new InputStreamReader(in1));

			    while ( (tekst = is1.readLine())  != null) 
			    {
			    	textArea.setText(textArea.getText()+tekst+"\n");
			    }
			    	
				String doSchowka = textArea.getText();
				System.out.println("textArea.getText() "+textArea.getText().toString());
				System.out.println(doSchowka);
				
				char [] konwenter1 = new char [doSchowka.length()];
				char [] konwenter2 = new char [doSchowka.length()];
				konwenter1 = doSchowka.toCharArray();
				int j=0;

				for(int i=0;i<doSchowka.length();i++)
				{
					if((int)konwenter1[i] > 32)
					{
						konwenter2[j] = konwenter1[i];
						j++;
					}
				}
				doSchowka = String.valueOf(konwenter2);
				System.out.println(doSchowka);
				StringSelection stringSelection = new StringSelection(doSchowka);
				schowek.setContents(stringSelection, null);                              
				schowek.setContents(stringSelection, null);
				is1.close();
				in1.close();
			}			
			else
			{
			
			InputStream inputStream = sock.getInputStream();

	        byte[] sizeAr = new byte[4];
	        inputStream.read(sizeAr);
	        int size = ByteBuffer.wrap(sizeAr).asIntBuffer().get();

	        byte[] imageAr = new byte[size];
	        inputStream.read(imageAr);
	        
	        BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageAr));
	        TransferableImage trans = new TransferableImage( image );
	        
	        schowek.setContents(trans, null);
	        panel = new PanelObraz(image);
	        SerwerRamka.contentPane.add(panel, BorderLayout.CENTER);
	        panel.repaint();
	        inputStream.close();
			}
			
			is.close();
			in.close();
			sock.close();
			sSock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Create the frame.
	 */
	public SerwerRamka() {
		setTitle("Serwer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		contentPane.add(panel, BorderLayout.CENTER);
		
		textArea = new JTextArea();
		textArea.setRows(2);
		contentPane.add(textArea, BorderLayout.NORTH);


	}

}
