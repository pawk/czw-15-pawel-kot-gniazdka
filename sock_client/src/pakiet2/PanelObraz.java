package pakiet2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class PanelObraz extends  JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	BufferedImage image;
	
	public PanelObraz () {
		
	}
	public PanelObraz (Object c) throws IOException{
	image = (BufferedImage) c ;

	Dimension dimension = new Dimension(image.getWidth(), image.getHeight());
	setPreferredSize(dimension);
	setBackground(Color.WHITE);
	ImageIO.write(image, "jpg", new File("C:\\Users\\USER\\Desktop\\test.jpg"));
	}
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(image, 0, 0, this);
	}
}
