package pakiet2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class RamkaKlient extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	PanelObraz panel_1 = new PanelObraz();
	Toolkit toolkit = Toolkit.getDefaultToolkit();
	Clipboard schowek = toolkit.getSystemClipboard();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RamkaKlient frame = new RamkaKlient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */ 
	
	public RamkaKlient() throws UnsupportedFlavorException, IOException{
		setTitle("Klient");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JTextArea textArea = new JTextArea();
		textArea.setRows(2);
		contentPane.add(textArea, BorderLayout.NORTH);
		
		JProgressBar progressBar = new JProgressBar(0,3);
		progressBar.setStringPainted(true);
		progressBar.setValue(0);
		
		
		JButton btnWylij = new JButton("Wy\u015Blij napis");
		btnWylij.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					//Socket sock = new Socket("10.104.35.133", 6666);
					Socket sock = new Socket("192.168.8.100", 6666);
					//Socket sock = new Socket("192.168.8.104", 80);
					//Socket sock = new Socket("192.168.8.1", 80);
					
					DataOutputStream so = new DataOutputStream(sock.getOutputStream());
					DataOutputStream so1 = new DataOutputStream(sock.getOutputStream());
					progressBar.setValue(1);
					
					progressBar.setValue(2);
				
					textArea.setText(schowek.getData(DataFlavor.stringFlavor).toString());
					so1.writeInt( 0);
					so.writeChars( schowek.getData(DataFlavor.stringFlavor).toString());
					
					progressBar.setValue(3);
					
					so.close();
					so1.close();
					sock.close();
				} catch (IOException | UnsupportedFlavorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JButton btnWylijObraz = new JButton("Wy\u015Blij obraz");
		btnWylijObraz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					panel_1 = new PanelObraz(schowek.getData(DataFlavor.imageFlavor));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedFlavorException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
					panel_1.setBackground(Color.WHITE);
					panel_1.repaint();
					progressBar.setValue(3);
					
					contentPane.add(panel_1, BorderLayout.CENTER);
					try {
					Socket sock = new Socket("192.168.8.100", 6666);
					//Socket sock = new Socket("192.168.8.104", 80);
					//Socket sock = new Socket("192.168.8.1", 80);
				       OutputStream outputStream = sock.getOutputStream();
				       DataOutputStream so1 = new DataOutputStream(sock.getOutputStream());
				       
				        BufferedImage image = (BufferedImage) schowek.getData(DataFlavor.imageFlavor);
				        
				        so1.writeInt( 1);
				        
				        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				        ImageIO.write(image, "jpg", byteArrayOutputStream);
				        
				        byte[] size = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
				        outputStream.write(size);
				        outputStream.write(byteArrayOutputStream.toByteArray());
				        outputStream.flush();
				        panel_1.repaint();
				        so1.close();
				        sock.close();
				        outputStream.close();
					} catch (IOException | UnsupportedFlavorException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		});
		panel.add(btnWylijObraz);
		panel.add(btnWylij);
		panel.add(progressBar);

		contentPane.add(panel_1, BorderLayout.CENTER);
		

	}

}
